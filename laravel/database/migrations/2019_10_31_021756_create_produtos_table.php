<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produtos_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->string('capa');
            $table->text('descricao');
            $table->decimal('valor', 10, 2)->nullable();
            $table->string('porcentagem_desconto');
            $table->decimal('valor_desconto', 10, 2)->nullable();
            $table->string('estoque');
            $table->boolean('vendido');
            $table->timestamp('vendido_em')->nullable();
            $table->foreign('produtos_categoria_id')->references('id')->on('produtos_categorias')->onDelete('set null');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('produtos');
        Schema::drop('produtos_categorias');
    }
}
