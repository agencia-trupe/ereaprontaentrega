@extends('frontend.common.template')

@section('content')

    <div class="produto">
        <div class="center">
            <div class="left">
                <div class="display">
                    <img src="{{ asset('assets/img/produtos/ampliacao/'.$produto->capa) }}" alt="">
                </div>

                @if(count($produto->imagens))
                <div class="thumbs">
                    <div class="active" data-image="{{ asset('assets/img/produtos/ampliacao/'.$produto->capa) }}">
                        <img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" alt="">
                    </div>
                    @foreach($produto->imagens as $imagem)
                    <div data-image="{{ asset('assets/img/produtos/ampliacao/'.$imagem->imagem) }}">
                        <img src="{{ asset('assets/img/produtos/'.$imagem->imagem) }}" alt="">
                    </div>
                    @endforeach
                </div>
                @endif

                <h2>{{ $produto->titulo }}</h2>
                <p>{!! $produto->valor_formatado !!}</p>

                @if($produto->vendido)
                <div class="vendido">
                    <span>VENDIDO</span>
                </div>
                @endif

                @if($produto->porcentagem_desconto)
                <div class="desconto">
                    <span>{{ $produto->porcentagem_desconto }}</span>
                </div>
                @endif
            </div>

            <div class="right">
                <h2>{{ $produto->titulo }}</h2>
                <p>{!! $produto->descricao !!}</p>

                <hr>

                <p class="valor">{{ Tools::formataValor($produto->valor_real) }}</p>

                @if(! $produto->vendido)
                <p class="estoque">Peças disponíveis: {{ $produto->estoque }}</p>

                @if(session('enviado'))
                <div class="enviado">
                    <div>
                        <span>Mensagem enviada com sucesso.</span>
                        <span>A Érea agradece o contato.</span>
                    </div>
                </div>
                @else
                <form action="{{ route('contato') }}" method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="produto_id" value="{{ $produto->id }}">
                    <input type="text" name="nome" value="{{ old('nome') }}" placeholder="nome do comprador" required>
                    <input type="email" name="email" value="{{ old('email') }}" placeholder="e-mail" required>
                    <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="telefone">
                    <input type="text" name="mensagem" value="{{ old('mensagem') }}" placeholder="comentário/observação">
                    <input type="submit" value="&raquo; COMPRAR">

                    @if($errors->any())
                    <div class="erro">Preencha todos os campos corretamente</div>
                    @endif
                </form>
                @endif

                <p class="estoque">Ao enviar sua solicitação nossa equipe entrará em contato para finalizar a aquisição. Aguarde seu contato via e-mail ou telefone.</p>
                @else
                <p class="estoque">Produto indisponível</p>
                @endif
            </div>
        </div>

        <div class="center">
            <a href="{{ route('home') }}" class="btn-voltar">
                VOLTAR PARA A VITRINE
            </a>
        </div>
    </div>


@endsection
