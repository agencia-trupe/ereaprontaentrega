    <footer>
        <div class="center">
            <p>© {{ date('Y') }} Érea &middot; Todos os direitos reservados.</p>
            <p>
                <a href="https://www.trupe.net" target="_blank">Criação de sites</a>:
                <a href="https://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
