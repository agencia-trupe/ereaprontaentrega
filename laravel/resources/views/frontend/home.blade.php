@extends('frontend.common.template')

@section('content')

    <div class="filtros">
        <div class="center">
            <div class="filtro">
                <span>produto:</span>
                <select name="categoria">
                    <option value="">TODOS</option>
                    @foreach($categorias as $categoria)
                    <option value="{{ $categoria->slug }}" @if(request('categoria') == $categoria->slug) selected @endif>{{ mb_strtoupper($categoria->titulo) }}</option>
                    @endforeach
                </select>
            </div>

            <div class="filtro">
                <span>ordenar por:</span>
                <select name="ordem">
                    <option value="relevancia" @if(request('ordem') == 'relevancia') selected @endif>RELEVÂNCIA</option>
                    <option value="valor-asc" @if(request('ordem') == 'valor-asc') selected @endif>MENOR PREÇO</option>
                    <option value="valor-desc" @if(request('ordem') == 'valor-desc') selected @endif>MAIOR PREÇO</option>
                    <option value="desconto" @if(request('ordem') == 'desconto') selected @endif>% OFF</option>
                </select>
            </div>
        </div>
    </div>

    <div class="produtos">
        <div class="center">
            @if(! count($produtos))
            <div class="nenhum">Nenhum produto encontrado.</div>
            @else
            @foreach($produtos as $produto)
                <a href="{{ route('produto', $produto->slug) }}">
                    <img src="{{ asset('assets/img/produtos/'.$produto->capa) }}" alt="">
                    <h2>{{ $produto->titulo }}</h2>
                    <p>{!! $produto->valor_formatado !!}</p>

                    @if($produto->vendido)
                    <div class="vendido">
                        <span>VENDIDO</span>
                    </div>
                    @endif

                    @if($produto->porcentagem_desconto)
                    <div class="desconto">
                        <span>{{ $produto->porcentagem_desconto }}</span>
                    </div>
                    @endif
                </a>
            @endforeach
            @endif
        </div>
    </div>

@endsection
