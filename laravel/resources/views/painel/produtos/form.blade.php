@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('produtos_categoria_id', 'Categoria') !!}
    {!! Form::select('produtos_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('valor', 'Valor') !!}
    @if(old('valor'))
    <input type="text" name="valor" class="form-control mask-valor" value="{{ str_replace('.', ',', old('valor')) }}">
    @else
    <input type="text" name="valor" class="form-control mask-valor" value="{{ isset($registro) ? str_replace('.', ',', $registro->valor) : '' }}">
    @endif
</div>

<div class="form-group">
    {!! Form::label('porcentagem_desconto', 'Porcentagem de desconto') !!}
    {!! Form::text('porcentagem_desconto', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('valor_desconto', 'Valor com desconto') !!}
    @if(old('valor_desconto'))
    <input type="text" name="valor_desconto" class="form-control mask-valor" value="{{ str_replace('.', ',', old('valor_desconto')) }}">
    @else
    <input type="text" name="valor_desconto" class="form-control mask-valor" value="{{ isset($registro) && $registro->valor_desconto > 0 ? str_replace('.', ',', $registro->valor_desconto) : '' }}">
    @endif
</div>

<div class="form-group">
    {!! Form::label('estoque', 'Estoque') !!}
    {!! Form::number('estoque', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="well">
    <div class="checkbox" style="margin:0">
        <label>
            <input type="hidden" name="vendido" value="0">
            {!! Form::checkbox('vendido', 1) !!}
            <span style="font-weight:bold">Vendido</span>
        </label>
    </div>
</div>

@if(isset($registro) && $registro->vendido && $registro->vendido_em)
<div class="row">
    <div class="col-md-6">
        <label>Vendido em</label>
        <div class="well">
            {{ $registro->vendido_em->format('d/m/Y') }}
        </div>
    </div>
    {{-- <div class="col-md-6">
        <label>Disponível no site até</label>
        <div class="well">
            {{ $registro->data_de_expiracao->format('d/m/Y') }}
        </div>
    </div> --}}
</div>
@endif

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
