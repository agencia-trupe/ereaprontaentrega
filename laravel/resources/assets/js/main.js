import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

const baseUrl = $('base').attr('href');

$('.filtro select').change(() => {
    const params = {};

    $('.filtro select').each(function() {
        const value = $(this).find('option:selected').attr('value');

        if (value) {
            params[$(this).attr('name')] = value;
        }
    });

    window.location = `${baseUrl}?${new URLSearchParams(params)}`;
});

$('.thumbs div').click(function(event) {
    event.preventDefault();

    const $this       = $(this);
    const $display    = $('.produto .display');
    const $displayImg = $display.find('img');

    if ($this.hasClass('active')) return;

    $('.thumbs div').removeClass('active');
    $this.addClass('active');

    $display.addClass('loading');
    $displayImg.on('load', () => $display.removeClass('loading'));
    $displayImg.attr('src', $this.data('image'));
});
