<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProdutoImagem extends Model
{
    protected $table = 'produtos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProduto($query, $id)
    {
        return $query->where('produto_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'       => 405,
                'height'      => 240,
                'transparent' => true,
                'path'        => 'assets/img/produtos/'
            ],
            [
                'width'       => 810,
                'height'      => 480,
                'transparent' => true,
                'path'        => 'assets/img/produtos/ampliacao/'
            ]
        ]);
    }
}
