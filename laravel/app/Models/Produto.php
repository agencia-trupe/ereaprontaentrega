<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Helpers\Tools;
use Carbon\Carbon;

class Produto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'produtos';

    protected $guarded = ['id'];

    protected $dates = ['vendido_em'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('produtos_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\ProdutoCategoria', 'produtos_categoria_id');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProdutoImagem', 'produto_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'       => 405,
                'height'      => 240,
                'transparent' => true,
                'path'        => 'assets/img/produtos/'
            ],
            [
                'width'       => 810,
                'height'      => 480,
                'transparent' => true,
                'path'        => 'assets/img/produtos/ampliacao/'
            ]
        ]);
    }

    public function getValorFormatadoAttribute()
    {
        if ($this->valor_desconto <= 0) {
            return Tools::formataValor($this->valor);
        }

        return '<span>de: '.Tools::formataValor($this->valor).' |</span> por: '.Tools::formataValor($this->valor_desconto);
    }

    public function getValorRealAttribute()
    {
        return $this->valor_desconto > 0 ? $this->valor_desconto : $this->valor;
    }

    public function getValorRealFormatadoAttribute()
    {
        $valor = $this->valor_desconto > 0 ? $this->valor_desconto : $this->valor;

        return Tools::formataValor($valor);
    }

    public function getDataDeExpiracaoAttribute()
    {
        return $this->vendido_em->addDays(10);
    }

    public function getOcultoAttribute()
    {
        if (! $this->vendido || ! $this->vendido_em) {
            return false;
        }

        return $this->data_de_expiracao->lt(Carbon::now()->startOfDay());
    }
}
