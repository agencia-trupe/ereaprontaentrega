<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'produtos_categoria_id' => 'required',
            'titulo' => 'required',
            'capa' => 'required|image',
            'descricao' => 'required',
            'valor' => 'required|regex:/^\d*(,\d{1,2})?$/',
            'porcentagem_desconto' => '',
            'valor_desconto' => 'regex:/^\d*(,\d{1,2})?$/',
            'estoque' => 'required|integer',
            'vendido' => '',
            'vendido_em' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
