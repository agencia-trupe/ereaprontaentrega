<?php

namespace App\Http\Controllers;

use App\Models\Produto;
use App\Models\ProdutoCategoria;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $categorias = ProdutoCategoria::ordenados()->get();

        $categoria = $request->get('categoria');
        $ordem = $request->get('ordem');

        $query = Produto::query();

        if (!! $categoria) {
            $query->whereHas('categoria', function($builder) use ($categoria) {
                $builder->where('slug', $categoria);
            });
        }

        if ($ordem == 'relevancia' || ! $ordem) {
            $query->ordenados();
        } elseif ($ordem === 'desconto') {
            $query->orderBy('porcentagem_desconto', 'desc');
        }

        $produtos = $query->get();

        if ($ordem == 'valor-asc') {
            $produtos = $produtos->sortBy('valor_real');
        } elseif ($ordem == 'valor-desc') {
            $produtos = $produtos->sortByDesc('valor_real');
        }

        return view('frontend.home', compact('produtos', 'categorias'));
    }

    public function show(Produto $produto) {
        return view('frontend.produto', compact('produto'));
    }
}
