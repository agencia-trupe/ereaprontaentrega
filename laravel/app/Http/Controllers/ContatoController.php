<?php

namespace App\Http\Controllers;

use App\Helpers\Tools;
use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;

use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Models\Produto;
use Illuminate\Support\Facades\Mail;

class ContatoController extends Controller
{
    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $input = $request->all();
        $produto = Produto::find($input['produto_id']);

        if (! $produto || $produto->vendido) return redirect()->route('home');

        $data = [
            'nome'           => $input['nome'],
            'email'          => $input['email'],
            'telefone'       => $input['telefone'],
            'mensagem'       => $input['mensagem'],
            'produto_titulo' => $produto->titulo,
            'produto_capa'   => asset('assets/img/produtos/'.$produto->capa),
            'produto_valor'  => Tools::formataValor($produto->valor_real),
            'produto_link'   => route('produto', $produto->slug),
        ];

        $contatoRecebido->create($data);

        $this->sendMail($data);

        return back()->with('enviado', true);
    }

    private function sendMail($data)
    {
        if (! $email = Contato::first()->email) {
           return false;
        }

        Mail::send('emails.contato', $data, function($m) use ($email, $data)
        {
            $m->to($email, config('app.name'))
               ->subject('[CONTATO] '.config('app.name'))
               ->replyTo($data['email'], $data['nome']);
        });
    }
}
